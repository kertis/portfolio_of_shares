<?php

namespace AppBundle\Services\StockPriceChartService;

use AppBundle\Entity\Stock;

class StockPriceAggregator
{
    /**
     * @var StockPriceHistoricalDataLoader
     */
    protected $loader;

    protected $stocks;

    /**
     * StockSeriesGenerator constructor.
     */
    public function __construct()
    {
        $this->loader = new StockPriceHistoricalDataLoader();
    }

    public function setStocks($stocks)
    {
        $this->stocks = $stocks;
    }

    public function calculateAllSeries()
    {
        $allSeries = [];
        foreach ($this->stocks as $stock) {
            $allSeries[] = $this->calculateSeries($stock);
        }
        $total = [];
        foreach ($allSeries as $series) {
            foreach ($series as $date => $price) {
                if (isset($total[$date])) {
                    $total[$date] += $price;
                }
                else $total[$date] = $price;
            }
        }
        return $total;
    }

    public function calculateSeries(Stock $stock)
    {
        $this->loader->setCode($stock->getCode());
        $this->loader->setCount($stock->getCount());
        return array_combine($this->loader->getDates(), $this->loader->getPrices());
    }
}
