<?php

namespace AppBundle\Services\StockPriceChartService;

use AppBundle\Entity\User;

class StockPriceChartService
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getChart()
    {
        if (!$this->getUser()) {
            throw new \BadMethodCallException('Initialize user');
        }
        $aggregator = new StockPriceAggregator();
        $aggregator->setStocks($this->getUser()->getStocks());
        $allSeries = array_reverse($aggregator->calculateAllSeries());
        $chart = new StockPriceChart();
        $chart->addSeries('Your portfolio', array_values($allSeries));
        $chart->setXAxisPoints(array_keys($allSeries));
        return $chart->getChart();
    }
}
