<?php

namespace AppBundle\Services\StockPriceChartService;

use Ob\HighchartsBundle\Highcharts\Highchart;

class StockPriceChart
{
	protected $series      = [];
	protected $xAxisPoints = [];

	public function addSeries($name, $data) {
		$this->series[] = ['name' => $name, 'data' => $data];
	}

    public function setXAxisPoints(array $points) {
        $this->xAxisPoints = $points;
    }

	public function getChart() {
        $ob = new Highchart();
        $ob->chart->renderTo('linechart'); 
        $ob->title->text('History data for your portfolio');
        $ob->xAxis->title(['text'  => "Time"]);
        $ob->xAxis->categories($this->xAxisPoints);
        $ob->yAxis->title(['text'  => "Price"]);
        $ob->series($this->series);
        return $ob;
	}
}
