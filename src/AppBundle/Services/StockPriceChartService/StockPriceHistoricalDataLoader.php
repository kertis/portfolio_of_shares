<?php

namespace AppBundle\Services\StockPriceChartService;

use \Scheb\YahooFinanceApi\ApiClient as YahooFinanceClient;
use Scheb\YahooFinanceApi\Exception\ApiException;
use Symfony\Component\Security\Acl\Exception\Exception;

class StockPriceHistoricalDataLoader
{
    protected $code;
    protected $count;
    protected $quotes;

    public function setCode($code)
    {
        $this->code   = $code;
        $this->quotes = null;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    protected function loadQuotes()
    {
        if ($this->code === null) {
            throw new \BadMethodCallException('Initialize code');
        }
        if ($this->count === null) {
            throw new \BadMethodCallException('Initialize count');
        }
        if ($this->quotes === null) {
            $client       = new YahooFinanceClient();
            $start        = date_create()->modify('-1 month');
            $end          = date_create();
            $this->quotes = [];
            foreach (range(0, 23) as $month) {
                try {
                    $data           = $client->getHistoricalData($this->code, $start, $end);
                    $this->quotes[] = $data['query']['results']['quote'][0];
                }
                catch (ApiException $e) {
                    // TODO should be logged? Throws an exception if earlier than certain date
                    break;
                }
                $start->modify('-1 month');
                $end->modify('-1 month');
            }
        }
        return $this->quotes;
    }

    public function getDates()
    {
        return array_map(function ($elem) {
            return $elem['Date'];
        }, $this->loadQuotes());
    }

    public function getPrices()
    {
        $count = $this->count; // workaround for 5.4
        return array_map(function ($elem) use ($count) {
            return $count * (float)$elem['Close'];
        }, $this->loadQuotes());
    }
}
