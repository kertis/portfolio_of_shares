portfolio_of_shares
===================

A Symfony project created on August 24, 2015, 9:29 am.

На исходной странице ссылки на логин и регистрация.
CRUD на акции по роуту /stock (пользователь автоматически туда попадает после логина).
name - название компании
code - код (GOOG, MSFT)
Посмотреть график можно по роуту /stock/graph или кликнув по ссылке из списка акций.

Использованы [FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle), [ObHighchartsBundle](https://github.com/marcaube/ObHighchartsBundle), [Yahoo Finance API](https://github.com/scheb/yahoo-finance-api)

Чтение Symfony Book, Cookbook, Best Practices - 5h
Авторизация и регистрация - 2h
Запрос данных с Yahoo - 2h
График - 3h

Запуск: 

`composer update`
`php app/console doctrine:scheme:update --force`
`php app/console server:start`
